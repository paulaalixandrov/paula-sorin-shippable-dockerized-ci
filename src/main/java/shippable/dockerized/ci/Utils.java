package shippable.dockerized.ci;

public class Utils {
    public static Boolean isLeapYear(int year) {
        if (year >= 1583) {
            return (year % 4 == 0) && year % 100 != 0 || year % 400 == 0;
        } else {
            return null;
        }
    }
}
