package shippable.dockerized.ci.tests;

import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.List;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;

public class TestREST {
    @Test
    public void testRomaniaCapitalName() {
        Response response = get("http://restcountries.eu/rest/v1/name/romania");
        assertThat(response.getStatusCode(), is(200));
        assertThat(response.getContentType(), is("application/json"));
        assertThat(JsonPath.read(response.asString(), "[0].capital"), is("Bucharest"));
    }

    @Test
    public void testRomaniaNumberOfNeighbors() {
        Response response = get("http://restcountries.eu/rest/v1/name/romania");
        assertThat(response.getStatusCode(), is(200));
        assertThat(response.getContentType(), containsString("application/json"));
        List<String> neighbors = JsonPath.read(response.asString(), "[0].borders");
        assertThat(neighbors.size(), is(5));
    }
}
