package shippable.dockerized.ci.tests;

import org.testng.annotations.Test;
import shippable.dockerized.ci.Utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

public class TestMisc {
    @Test
    public void test1582LeapYear() {
        assertThat(Utils.isLeapYear(1582), is(nullValue()));
    }

    @Test
    public void test1600LeapYear() {
        assertThat(Utils.isLeapYear(1600), is(true));
    }

    @Test
    public void test2009LeapYear() {
        assertThat(Utils.isLeapYear(2009), is(false));
    }
}
